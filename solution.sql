/* 
CREATE DATABASE dbrelational;

 CREATE TABLE author(
    au_id VARCHAR(50) NOT NULL,
    au_lname VARCHAR(50) NOT NULL,
    au_fname VARCHAR(50) NOT NULL,
    address VARCHAR(50) NOT NULL,
    city VARCHAR(50) NOT NULL,
    state VARCHAR(25) NOT NULL,
    PRIMARY KEY(au_id)
);

CREATE TABLE publisher( 
    pub_id INT NOT NULL,
    pub_name VARCHAR(50) NOT NULL,
    city VARCHAR(25) NOT NULL,
    PRIMARY KEY(pub_id)
);

CREATE TABLE title( 
    title_id VARCHAR(50) NOT NULL,
    title VARCHAR(50) NOT NULL,
    type VARCHAR(50) NOT NULL,
    price DECIMAL(10,2) NOT NULL,
    pub_id INT NOT NULL,
    PRIMARY KEY(title_id),
    CONSTRAINT fk_title_publisher_id
        FOREIGN KEY(pub_id) REFERENCES publisher(pub_id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE author_title( 
    au_id VARCHAR(50) NOT NULL,
    title_id VARCHAR(50) NOT NULL,
    CONSTRAINT fk_author_title_author_id
        FOREIGN KEY (au_id) REFERENCES author(au_id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_author_title_title_id
        FOREIGN KEY (title_id) REFERENCES title(title_id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);    
*/
2. JOSHUA G. ORBISO BSIT-3 | G1

a. 
SELECT *  from title 
    INNER JOIN author_title ON title.title_id = author_title.title_id
    WHERE au_id = '213-46-8915';

b.
SELECT *  from title 
    INNER JOIN author_title ON title.title_id = author_title.title_id
    WHERE au_id = '267-41-2394';

c.
SELECT *  from author
    INNER JOIN author_title ON author.au_id = author_title.au_id
    WHERE title_id = 'BU1032';

d.
SELECT *  from publisher
    INNER JOIN title ON publisher.pub_id = title.pub_id
    WHERE title_id = 'PC1035';

e.
SELECT *  from title 
    INNER JOIN publisher ON title.pub_id = publisher.pub_id
    WHERE pub_id = '1398';

3.
CREATE DATABASE blog_db;

CREATE TABLE post(
    id INT NOT NULL,
    author_id INT NOT NULL,
    title VARCHAR(500) NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY(id)  
);

CREATE TABLE post_comments(
    id INT NOT NULL,
    post_id INT NOT NULL,
    user_id INT NOT NULL,    
    content VARCHAR(5000) NOT NULL,
    datetime_commented DATETIME NOT NULL,
    PRIMARY KEY(id)   
    CONSTRAINT fk_post_post_comment_id
        FOREIGN KEY (post_id) REFERENCES post(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,       
    CONSTRAINT fk_post_comment_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,  
);

CREATE TABLE post_likes(
    id INT NOT NULL,
    post_id INT NOT NULL,
    user_id INT NOT NULL,    
    datetime_liked DATETIME NOT NULL,
    PRIMARY KEY(id)   
    CONSTRAINT fk_post_post_comment_id
        FOREIGN KEY (post_id) REFERENCES post(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,       
    CONSTRAINT fk_post_comment_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,  
);

CREATE TABLE users(
    id INT NOT NULL,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(300) NOT NULL,
    datetime_created DATETIME NOT NULL,
)

SELECT * FROM post p
INNER JOIN post_comment pc  
  ON p.id = pc.post_id;

SELECT * FROM users u 
INNER JOIN post_comments pc  
  ON u.id = pc.user_id;

SELECT * FROM post p
INNER JOIN post_likes pl   
  ON p.id = pl.post_id;

SELECT * FROM users u 
INNER JOIN post_likes pl  
  ON u.id = pl.user_id;